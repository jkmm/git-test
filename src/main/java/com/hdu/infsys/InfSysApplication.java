package com.hdu.infsys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(InfSysApplication.class, args);
    }

}
