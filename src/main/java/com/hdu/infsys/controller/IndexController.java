package com.hdu.infsys.controller;



import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.Map;


@Slf4j
@Controller
public class IndexController {

    @GetMapping("/index1")
    public String login(){
        return "/index1";
    }

    @GetMapping(value = {"/","/login"})
    public String loginPage(){
        return "test";
    }


    @GetMapping("login2")
    public String l(){
        return "/index2";
    }

    @ResponseBody
    @GetMapping("/login1")
    public Map login1(@RequestParam("account")String acc,
                      @RequestParam("passWord")String pwd){
        System.out.println("==================================");
        System.out.println(acc+"==="+pwd);
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("isLogin","true");
        return objectObjectHashMap;
    }

}
