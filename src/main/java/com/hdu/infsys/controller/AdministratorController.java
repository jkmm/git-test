package com.hdu.infsys.controller;


import com.hdu.infsys.bean.Administrator;
import com.hdu.infsys.bean.Msg;
import com.hdu.infsys.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdministratorController {

    @Autowired
    AdministratorService administratorService;

    @PutMapping(value = "/administrator/{id}")
    @ResponseBody
    public Msg updateAdministrator(Administrator administrator) {
        System.out.println(administrator.toString());
        administratorService.saveOrUpdate(administrator);
        return Msg.success();
    }


}
