package com.hdu.infsys.controller;

import com.hdu.infsys.bean.Msg;
import com.hdu.infsys.bean.User;
import com.hdu.infsys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @ResponseBody
    @GetMapping("/user")
    public ModelAndView test(ModelAndView model){
        User user = userService.getById(1);
        model.addObject("user",user);
        model.setViewName("test");
        return model;
    }

    @DeleteMapping(value = "/user/{id}")
    @ResponseBody
    public Msg deleteUserById(@PathVariable("id")String id) {
        userService.removeById(Integer.parseInt(id));
        return Msg.success();
    }

    @PutMapping(value = "/user/{id}")
    @ResponseBody
    public Msg updateUser(User user, HttpServletRequest request) {
        userService.saveOrUpdate(user);
        return Msg.success();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Msg getUser(@PathVariable("id") Integer id) {
        User user = userService.getById(id);
        return Msg.success().add("user", user);
    }

    @PostMapping(value = "/user")
    @ResponseBody
    public Msg saveUser(@Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, Object> map = new HashMap<String, Object>();
            List<FieldError> fieldErrors = result.getFieldErrors();
            for (FieldError fieldError : fieldErrors) {
                System.out.println("错误的字段名："+fieldError.getField());
                System.out.println("错误信息："+fieldError.getDefaultMessage());
                map.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            return Msg.fail().add("errorFields", map);
        }else {
            userService.saveOrUpdate(user);
            return Msg.success();
        }
    }

    @RequestMapping("/checkUser")
    @ResponseBody
    public Msg checkUser(@RequestParam("userName")String userName) {
        String regx = "(^[a-zA-Z0-9_-]{6,16}$)|(^[\u2E80-\u9FFF]{2,5})";
        if(!userName.matches(regx)){
            return Msg.fail().add("va_msg", "用户名必须是6-16位数字和字母的组合或者2-5位中文");
        }
        boolean checkUser = userService.checkUser(userName);
        if (checkUser) {
            return Msg.success();
        }else {
            return Msg.fail().add("va_msg", "用户名不可用");
        }
    }



}
