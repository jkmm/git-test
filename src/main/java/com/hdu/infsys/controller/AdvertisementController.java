package com.hdu.infsys.controller;


import com.hdu.infsys.bean.Advertisement;
import com.hdu.infsys.bean.Msg;
import com.hdu.infsys.service.AdvertisementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class AdvertisementController {

    @Autowired
    AdvertisementService advertisementService;

    @DeleteMapping(value = "/ads/{id}")
    public Msg deleteAdvertisementById(@PathVariable("id") String id) {
        advertisementService.removeById(Integer.parseInt(id));
        return Msg.success();
    }

    @PutMapping(value = "/ads/{id}")
    public Msg updateAdvertisement(Advertisement advertisement) {
        log.info(advertisement.toString());
        advertisementService.saveOrUpdate(advertisement);
        return Msg.success();
    }

    @RequestMapping(value = "/ads/{id}", method = RequestMethod.GET)
    public Msg getAdvertisement(@PathVariable("id") Integer id) {
        Advertisement advertisement= advertisementService.getById(id);
        return Msg.success().add("advertisement", advertisement);
    }

    @PostMapping(value = "/ads")
    public Msg saveAdvertisement(Advertisement advertisement) {
        advertisementService.saveOrUpdate(advertisement);
        return Msg.success();
    }

}
