package com.hdu.infsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdu.infsys.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select count(*) from user where user_name = #{name}")
    public Long countByName(String name);

}
