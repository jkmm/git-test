package com.hdu.infsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdu.infsys.bean.Advertisement;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdvertisementMapper extends BaseMapper<Advertisement> {
}
