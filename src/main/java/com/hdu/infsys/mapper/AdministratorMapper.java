package com.hdu.infsys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdu.infsys.bean.Administrator;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdministratorMapper extends BaseMapper<Administrator> {
}
