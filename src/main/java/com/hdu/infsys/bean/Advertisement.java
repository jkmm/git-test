package com.hdu.infsys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Advertisement {

    private Integer id;
    private String address;
    private String picture;
    private String price;
    private String description;
    private Integer publisherId;
}
