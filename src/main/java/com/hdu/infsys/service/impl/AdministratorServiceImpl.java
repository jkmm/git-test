package com.hdu.infsys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdu.infsys.bean.Administrator;
import com.hdu.infsys.mapper.AdministratorMapper;
import com.hdu.infsys.service.AdministratorService;
import org.springframework.stereotype.Service;

@Service
public class AdministratorServiceImpl extends ServiceImpl<AdministratorMapper, Administrator> implements AdministratorService {
}
