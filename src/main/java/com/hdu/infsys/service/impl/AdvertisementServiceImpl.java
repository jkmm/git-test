package com.hdu.infsys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdu.infsys.bean.Advertisement;
import com.hdu.infsys.mapper.AdvertisementMapper;
import com.hdu.infsys.service.AdvertisementService;
import org.springframework.stereotype.Service;

@Service
public class AdvertisementServiceImpl extends ServiceImpl<AdvertisementMapper, Advertisement> implements AdvertisementService {

}
