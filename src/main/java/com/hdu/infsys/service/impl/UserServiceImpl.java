package com.hdu.infsys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdu.infsys.bean.User;
import com.hdu.infsys.mapper.UserMapper;
import com.hdu.infsys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public boolean checkUser(String userName) {
        Long aLong = userMapper.countByName(userName);
        return aLong==0L;
    }
}
