package com.hdu.infsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdu.infsys.bean.Advertisement;

public interface AdvertisementService extends IService<Advertisement> {
}
