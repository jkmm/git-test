package com.hdu.infsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdu.infsys.bean.Administrator;

public interface AdministratorService extends IService<Administrator> {
}
