package com.hdu.infsys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdu.infsys.bean.User;

public interface UserService extends IService<User> {

    public boolean checkUser(String userName);

}
