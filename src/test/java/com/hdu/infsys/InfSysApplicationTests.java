package com.hdu.infsys;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

@Slf4j
@SpringBootTest
class InfSysApplicationTests {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    void contextLoads() {
        String s = jdbcTemplate.queryForObject("select name from test where id = 1", String.class);
        log.info("记录：{}",s);
//        log.info("数据源类型：{}",dataSource.getClass());
    }

}
